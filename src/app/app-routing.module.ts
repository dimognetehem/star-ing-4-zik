import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'accueil',
    loadChildren: () => import('./accueil/accueil.module').then( m => m.AccueilPageModule)
  },
  {
    path: 'tabs/musique',
    loadChildren: () => import('./musique/musique.module').then( m => m.MusiquePageModule)
  },
  {
    path: 'apropos',
    loadChildren: () => import('./apropos/apropos.module').then( m => m.AproposPageModule)
  },
  {
    path: 'categorie/:id',
    loadChildren: () => import('./categorie/categorie.module').then( m => m.CategoriePageModule)
  },
  {
    path: 'lecture/:id',
    loadChildren: () => import('./lecture/lecture.module').then( m => m.LecturePageModule)
  },
  {
    path: 'tabs/musique/artistes',
    loadChildren: () => import('./artistes/artistes.module').then( m => m.ArtistesPageModule)
  },
  {
    path: 'tabs/musique/albums',
    loadChildren: () => import('./albums/albums.module').then( m => m.AlbumsPageModule)
  },
  {
    path: 'tabs/musique/chansons',
    loadChildren: () => import('./chansons/chansons.module').then( m => m.ChansonsPageModule)
  },
  {
    path: 'chansonsparart/:nomart',
    loadChildren: () => import('./chansonsparart/chansonsparart.module').then( m => m.ChansonsparartPageModule)
  },
  {
    path: 'chansonsparalbum/:nomalb',
    loadChildren: () => import('./chansonsparalbum/chansonsparalbum.module').then( m => m.ChansonsparalbumPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
