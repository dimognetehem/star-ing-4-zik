import { Component, OnInit } from '@angular/core';
import {MusicService} from "../services/music.service";

@Component({
  selector: 'app-albums',
  templateUrl: './albums.page.html',
  styleUrls: ['./albums.page.scss'],
})
export class AlbumsPage implements OnInit {

  albums : any[] = [];

  ispending:boolean = true;

  constructor(private musicService: MusicService) { }

  ngOnInit() {

    this.musicService.getListOfSongs().subscribe((reponse: any) => {
      // @ts-ignore
      this.albums = reponse.filter(a => a.album.nomAlbum != "<Pas d\'album>").sort((a,b) => a.album.nomAlbum > b.album.nomAlbum ? 1 : -1);

      for(var i= 0;i<this.albums.length-1;i++){
        if(this.albums[i].album.nomAlbum == this.albums[i+1].album.nomAlbum){
          this.albums.splice(i,1);
        }
      }
    });

    setTimeout(() => {
      this.ispending = false;
    }, 1500);


  }

}
