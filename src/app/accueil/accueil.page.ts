import {Component, OnInit} from '@angular/core';
import {image} from "ionicons/icons";
import {CategorieService} from "../services/categorie.service";
import {MusicService} from "../services/music.service";
import {LoadingController} from "@ionic/angular";


@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.page.html',
  styleUrls: ['./accueil.page.scss'],
})
export class AccueilPage implements OnInit {

  categories: any[] = [];

  songs: any[] = [];

  songsfav : any[] = [];

  songswithhighscore : any[] = [];

  ispending:boolean = true;

  constructor(private categorieService: CategorieService, private musicService: MusicService, private loadCtrl : LoadingController) {

  }


  async ngOnInit() {
    await this.presentLoading();
    this.categorieService.getListOfCategories().subscribe((reponse: any) => {
      this.categories = reponse;
    });
    this.musicService.getListOfSongs().subscribe((reponse1: any) => {
      this.songs = reponse1;
      // @ts-ignore
      this.songsfav = reponse1.filter(a => a.est_favoris == true);
      // @ts-ignore
      this.songswithhighscore = reponse1.filter(a => a.scoreArtiste > 7).sort((a, b) => a.scoreArtiste < b.scoreArtiste ? 1 : -1);
    });

    setTimeout(() => {
      this.ispending = false;
    }, 5000);


  }

  async presentLoading(){
    const loading = await this.loadCtrl.create({
      message: "Chargement des données",
      duration: 4500,
      spinner: "lines"
    });
    await loading.present();
  }




  protected readonly image = image;
}
