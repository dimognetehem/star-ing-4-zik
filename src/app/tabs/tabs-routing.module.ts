import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'accueil',
        loadChildren: () => import('../accueil/accueil.module').then(m => m.AccueilPageModule)
      },
      {
        path: 'musique',
        loadChildren: () => import('../musique/musique.module').then(m => m.MusiquePageModule)
      },
      {
        path: 'apropos',
        loadChildren: () => import('../apropos/apropos.module').then(m => m.AproposPageModule)
      },
      {
        path: 'musique/artistes',
        loadChildren: () => import('../artistes/artistes.module').then(m => m.ArtistesPageModule)
      },
      {
        path: 'musique/albums',
        loadChildren: () => import('../albums/albums.module').then(m => m.AlbumsPageModule)
      },
      {
        path: 'musique/chansons',
        loadChildren: () => import('../chansons/chansons.module').then(m => m.ChansonsPageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/accueil',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/accueil',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
