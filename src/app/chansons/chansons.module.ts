import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChansonsPageRoutingModule } from './chansons-routing.module';

import { ChansonsPage } from './chansons.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChansonsPageRoutingModule
  ],
  declarations: [ChansonsPage]
})
export class ChansonsPageModule {}
