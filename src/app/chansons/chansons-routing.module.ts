import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChansonsPage } from './chansons.page';

const routes: Routes = [
  {
    path: '',
    component: ChansonsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChansonsPageRoutingModule {}
