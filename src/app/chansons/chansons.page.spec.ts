import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ChansonsPage } from './chansons.page';

describe('ChansonsPage', () => {
  let component: ChansonsPage;
  let fixture: ComponentFixture<ChansonsPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ChansonsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
