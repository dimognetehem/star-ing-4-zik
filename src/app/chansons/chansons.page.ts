import { Component, OnInit } from '@angular/core';
import {MusicService} from "../services/music.service";
import {Filesystem, Directory, FilesystemDirectory} from '@capacitor/filesystem';
import {ActivatedRoute, Router} from "@angular/router";
const APP_DIRECTORY = Directory.Documents;
@Component({
  selector: 'app-chansons',
  templateUrl: './chansons.page.html',
  styleUrls: ['./chansons.page.scss'],
})
export class ChansonsPage implements OnInit {

  chansons : any[] = [];

  ispending:boolean = true;
  folderContent = [];
  currentFolder = '';

  constructor(private musicService: MusicService, private route : ActivatedRoute) { }

  ngOnInit() {
    this.musicService.getListOfSongs().subscribe((reponse: any) => {
      // @ts-ignore
      this.chansons = reponse.sort((a,b) => a.titre > b.titre ? 1 : -1);


    });

    setTimeout(() => {
      this.ispending = false;
    }, 1500);

    // this.currentFolder = this.route.snapshot.paramMap.get('folder') || '';
    // this.loadDocuments().then(r => "test");
  }

    async listFiles() {
    try {
      const directory: any = Directory.Documents; // Répertoire à lire (ex: Documents, Data, Cache, etc.)
      const result = await Filesystem.readdir({
        path: APP_DIRECTORY,
      });
      console.log('Liste des fichiers et répertoires :', result.files);
    } catch (error) {
      console.error('Erreur lors de la lecture des fichiers :', error);
    }
  }
  // async loadDocuments() {
  //   try {
  //   const folderContent = await Filesystem.readdir({
  //     directory: APP_DIRECTORY,
  //     path: "C:\\Users\\SAMSUNG\\Documents\\folder"
  //   });
  //   console.log(folderContent);
  //   console.log('Liste des fichiers et répertoires :', folderContent.files);
  // } catch (error) {
  //   console.error('Erreur lors de la lecture des fichiers :', error);
  // }
  //
  //   // The directory array is just strings
  //   // We add the information isFile to make life easier
  //
  //
  // }




}
