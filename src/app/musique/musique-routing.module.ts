import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MusiquePage } from './musique.page';

const routes: Routes = [
  {
    path: '',
    component: MusiquePage,
    children: [
      {
        path: 'musique/artistes',
        loadChildren: () => import('../artistes/artistes.module').then(m => m.ArtistesPageModule)
      },
      {
        path: 'musique/albums',
        loadChildren: () => import('../albums/albums.module').then(m => m.AlbumsPageModule)
      },
      {
        path: 'musique/chansons',
        loadChildren: () => import('../chansons/chansons.module').then(m => m.ChansonsPageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/musique/artistes',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/musique',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MusiquePageRoutingModule {}
