import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChansonsparartPageRoutingModule } from './chansonsparart-routing.module';

import { ChansonsparartPage } from './chansonsparart.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChansonsparartPageRoutingModule
  ],
  declarations: [ChansonsparartPage]
})
export class ChansonsparartPageModule {}
