import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ChansonsparartPage } from './chansonsparart.page';

describe('ChansonsparartPage', () => {
  let component: ChansonsparartPage;
  let fixture: ComponentFixture<ChansonsparartPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ChansonsparartPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
