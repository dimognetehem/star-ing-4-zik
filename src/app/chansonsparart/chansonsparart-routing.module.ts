import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChansonsparartPage } from './chansonsparart.page';

const routes: Routes = [
  {
    path: '',
    component: ChansonsparartPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChansonsparartPageRoutingModule {}
