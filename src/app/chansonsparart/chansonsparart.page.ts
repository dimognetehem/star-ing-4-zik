import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {MusicService} from "../services/music.service";

@Component({
  selector: 'app-chansonsparart',
  templateUrl: './chansonsparart.page.html',
  styleUrls: ['./chansonsparart.page.scss'],
})
export class ChansonsparartPage implements OnInit {
  nomArt : string|any;
  songsparart : any[] = [];

  constructor(private musicService: MusicService, private router: ActivatedRoute) {
    this.nomArt = this.router.snapshot.paramMap.get("nomart");
  }

  ngOnInit() {
    this.musicService.getListOfSongs().subscribe((reponse: any) => {
      // @ts-ignore
      this.songsparart = reponse.filter(a => a.nomArtiste == this.nomArt).sort((a,b) => a.titre > b.titre ? 1 : -1);


    });
  }

}
