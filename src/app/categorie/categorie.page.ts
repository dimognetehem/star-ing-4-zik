import { Component, OnInit } from '@angular/core';
import {MusicService} from "../services/music.service";
import {ActivatedRoute} from "@angular/router";
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-categorie',
  templateUrl: './categorie.page.html',
  styleUrls: ['./categorie.page.scss'],
})
export class CategoriePage implements OnInit {

  idCat: number|any;
  chansons: any[] = [];
  chansonsfilter: any[] = [];
  nomCat: string|any;
  searchControl: string = '';
  ispending:boolean = true;
  issearching:boolean = false;

  constructor(private musicService: MusicService, private router: ActivatedRoute) {
    this.idCat = this.router.snapshot.paramMap.get("id");

    if(this.idCat == 1){
      this.nomCat = "Benskin";
    }
    else if(this.idCat == 2){
      this.nomCat = "Bikutsi";

    }
    else if(this.idCat == 3){
      this.nomCat = "Makossa";
    }
    else if(this.idCat == 4){
      this.nomCat = "AfroUrban";
    }
    else {
      this.nomCat = "RnB";
    }

  }

  ngOnInit() {
    this.musicService.getListOfSongs().subscribe((reponse: any) => {
      // @ts-ignore
      this.chansons = reponse.filter(a => a.idCategorie == this.idCat).sort((a,b) => a.titre > b.titre ? 1 : -1);

      this.chansonsfilter = this.chansons;

    });

    setTimeout(() => {
      this.ispending = false;
    }, 2000);

  }
  filterSongs(query: string) {
    this.issearching = true;
    this.chansonsfilter = this.chansons.filter(d => d.titre.toLowerCase().indexOf(query) > -1 || d.album.nomAlbum.toLowerCase().indexOf(query) > -1);

    setTimeout(() => {
      this.issearching = false;
    }, 2000);
  }

  handleInput() {
    this.filterSongs(this.searchControl.toLowerCase());
  }

  // handleInput($event: any) {
  //   // @ts-ignore
  //   const query = event.target.value.toLowerCase();
  //   this.chansonsfilter = this.chansons.filter((d) => d.toLowerCase().indexOf(query) > -1);
  //
  // }
}
