import {Component, OnInit} from '@angular/core';
import {MusicService} from "../services/music.service";

@Component({
  selector: 'app-artistes',
  templateUrl: './artistes.page.html',
  styleUrls: ['./artistes.page.scss'],
})
export class ArtistesPage implements OnInit {


  artistes : any[] = [];
  ispending:boolean = true;

  constructor(private musicService: MusicService) { }

  ngOnInit() {

    this.musicService.getListOfSongs().subscribe((reponse: any) => {
      // @ts-ignore
      this.artistes = reponse;


      for(var i= 0;i<this.artistes.length-1;i++){
        if(this.artistes[i].nomArtiste == this.artistes[i+1].nomArtiste){
          this.artistes.splice(i,1);
        }
      }

    });
    setTimeout(() => {
      this.ispending = false;
    }, 1500);

  }

}
