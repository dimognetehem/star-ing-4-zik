import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArtistesPage } from './artistes.page';

const routes: Routes = [
  {
    path: '',
    component: ArtistesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ArtistesPageRoutingModule {}
