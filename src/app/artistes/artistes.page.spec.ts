import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ArtistesPage } from './artistes.page';

describe('ArtistesPage', () => {
  let component: ArtistesPage;
  let fixture: ComponentFixture<ArtistesPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ArtistesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
