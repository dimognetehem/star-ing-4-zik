import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ArtistesPageRoutingModule } from './artistes-routing.module';

import { ArtistesPage } from './artistes.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ArtistesPageRoutingModule
  ],
  declarations: [ArtistesPage]
})
export class ArtistesPageModule {}
