import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChansonsparalbumPageRoutingModule } from './chansonsparalbum-routing.module';

import { ChansonsparalbumPage } from './chansonsparalbum.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChansonsparalbumPageRoutingModule
  ],
  declarations: [ChansonsparalbumPage]
})
export class ChansonsparalbumPageModule {}
