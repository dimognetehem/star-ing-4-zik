import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChansonsparalbumPage } from './chansonsparalbum.page';

const routes: Routes = [
  {
    path: '',
    component: ChansonsparalbumPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChansonsparalbumPageRoutingModule {}
