import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ChansonsparalbumPage } from './chansonsparalbum.page';

describe('ChansonsparalbumPage', () => {
  let component: ChansonsparalbumPage;
  let fixture: ComponentFixture<ChansonsparalbumPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ChansonsparalbumPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
