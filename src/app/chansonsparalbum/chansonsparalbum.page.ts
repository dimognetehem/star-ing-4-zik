import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {MusicService} from "../services/music.service";

@Component({
  selector: 'app-chansonsparalbum',
  templateUrl: './chansonsparalbum.page.html',
  styleUrls: ['./chansonsparalbum.page.scss'],
})
export class ChansonsparalbumPage implements OnInit {

  nomAlb : string|any;
  songsparalb: any[] = [];

  constructor(private musicService: MusicService, private router: ActivatedRoute) {
    this.nomAlb = this.router.snapshot.paramMap.get("nomalb");


  }

  ngOnInit() {
    this.musicService.getListOfSongs().subscribe((reponse: any) => {
      // @ts-ignore
      this.songsparalb = reponse.filter(a => a.album.nomAlbum == this.nomAlb).sort((a,b) => a.titre > b.titre ? 1 : -1);

    });
  }

}
