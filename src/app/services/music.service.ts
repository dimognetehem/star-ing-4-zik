import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";


@Injectable({
  providedIn: 'root'
})
export class MusicService {

  constructor(private http : HttpClient) { }

  getListOfSongs(){
    return this.http.get("assets/songs.json");
  }
}
