import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";


@Injectable({
  providedIn: 'root'
})
export class CategorieService {

  constructor(private http : HttpClient) {
  }

  getListOfCategories(){
    return this.http.get("assets/categories.json");
  }
}
