import {Component, OnInit} from '@angular/core';
import {MusicService} from "../services/music.service";
import {ActivatedRoute, Router} from "@angular/router";
import {NativeAudio} from "@capacitor-community/native-audio";
import {Directory, Filesystem} from "@capacitor/filesystem";

@Component({
  selector: 'app-lecture',
  templateUrl: './lecture.page.html',
  styleUrls: ['./lecture.page.scss'],
})
export class LecturePage implements OnInit {
  idSong:number|any;
  tempo:number|any;
  encours : string|any;
  isPlay: boolean|any;
  isUnload: boolean = false;
  song : any;
  path: string|any;
  filename : string = "";
  currentTime:number = 0;
  audioDuration:number|any;
  audioTime : string = "00:00";
  volume:number = 0.5;


  constructor(private musicService: MusicService, private router: ActivatedRoute, private route : Router) {
    this.idSong = this.router.snapshot.paramMap.get("id");


  }

     ngOnInit() {
      this.musicService.getListOfSongs().subscribe((reponse: any) => {

        // @ts-ignore
        this.song = reponse.filter(a => a.idChanson == this.idSong);

        // Filesystem.getUri({
        //   path: "zik_Ing4",
        //   directory: Directory.Documents
        // }).then((result) => {
        //   console.log(result.uri);
        // });

        const value: number = this.song[0].idCategorie;

        switch (value) {
          case 1:
            this.path = "benskin";
            break;
          case 2:
            this.path = "bikutsi";
            break;
          case 3:
            this.path = "makossa";
            break;
          case 4:
            this.path = "afrourban";
            break;
          default:
            this.path = "rnb";
            break;
        }

        // NativeAudio.preload({
        //   assetId: "assetId",  //Identifiant unique du fichier
        //   assetPath: "file:///storage/emulated/0/Documents/zik_Ing4/"+this.path+"/"+this.song[0].nomfichier,  //chemin relatif ou absolue (file://)
        //   audioChannelNum: 1,
        //   isUrl: true  //Si oui ou non le chemin du fichier est une URL
        // }).then(result => {
        //   //console.log("song preload");
        // });

        // this.audioDuration = this.audioDuration;

      });


    }

  convertDuration(secondes : number){
    const min = Math.floor((secondes/60/60)*60);
    const sec = Math.floor(((secondes/60/60)*60 - min)*60);
    const time = '0'+min+':'+sec;
    return time;

  }

  async play() {

    if(!this.isPlay && this.currentTime==0){
      await NativeAudio.preload({
        assetId: "assetId",
        assetPath: "file:///storage/emulated/0/Documents/zik_Ing4/" + this.path + "/" + this.song[0].nomfichier,
        audioChannelNum: 1,
        isUrl: true  //Si oui ou non le chemin du fichier est une URL
      });

      await NativeAudio.play({
        assetId: "assetId",  //Identifiant unique du fichier
      });

    }
    if(!this.isPlay && this.currentTime > 0){
      await NativeAudio.resume({
        assetId: "assetId",  //Identifiant unique du fichier
      });
    }



    setInterval(() => {
      this.tempo = NativeAudio.getCurrentTime({
        assetId: "assetId"
      })
        .then(result => {

          NativeAudio.isPlaying({
            assetId: "assetId"
          })
            .then(result => {
              this.isPlay = result.isPlaying
              //console.log(this.isPlay);
            });

          let min = 0, sec = 0

          this.currentTime = Math.floor(result.currentTime);

          if (this.currentTime < 60) {
            sec = this.currentTime;
            this.encours = '0' + min + ':' + sec;
          } else if (this.currentTime >= 60 && this.currentTime < 120) {
            sec = this.currentTime - 60;
            min += 1;
            this.encours = '0' + min + ':' + sec;
          } else if (this.currentTime >= 120 && this.currentTime < 180) {
            sec = this.currentTime - 120;
            min += 2;
            this.encours = '0' + min + ':' + sec;
          } else if (this.currentTime >= 180 && this.currentTime < 240) {
            sec = this.currentTime - 180;
            min += 3;
            this.encours = '0' + min + ':' + sec;
          } else if (this.currentTime >= 240 && this.currentTime < 300) {
            sec = this.currentTime - 240;
            min += 4;
            this.encours = '0' + min + ':' + sec;
          } else if (this.currentTime >= 300 && this.currentTime < 360) {
            sec = this.currentTime - 300;
            min += 5;
            this.encours = '0' + min + ':' + sec;
          }

          //console.log(this.currentTime);
          //console.log(Math.floor(this.audioDuration));
          if (this.currentTime == Math.floor(this.audioDuration)) {
            this.stop();
          }
        })
    }, 500);


    const play = document.getElementById("play");
    // @ts-ignore
    play.style.display = "none";
    const pause = document.getElementById("pause");
    // @ts-ignore
    pause.style.display = "inherit";

  }

  playAtTime(ev: any){
    NativeAudio.play({
      assetId: "assetId",  //Identifiant unique du fichier
      time : ev.detail.value
    });
    const play = document.getElementById("play");
    // @ts-ignore
    play.style.display = "none";
    const pause = document.getElementById("pause");
    // @ts-ignore
    pause.style.display = "inherit";

  }

  pause(){

      NativeAudio.pause({
        assetId: "assetId"
      });
      const play = document.getElementById("play");
      // @ts-ignore
      play.style.display = "inherit";
      const pause = document.getElementById("pause");
      // @ts-ignore
      pause.style.display = "none";
      //console.log(play);

  }
  stop() {
    this.currentTime = 0;

      NativeAudio.stop({
        assetId: "assetId"
      });
      NativeAudio.unload({
        assetId: "assetId"
      });
      const play = document.getElementById("play");
      // @ts-ignore
      play.style.display = "inherit";
      const pause = document.getElementById("pause");
      // @ts-ignore
      pause.style.display = "none";

    this.isPlay = false;
  }

  voldown(){

    this.volume = this.volume - 0.1;
    if(this.volume >= 0.1){
      NativeAudio.setVolume({
        assetId: "assetId",
        volume: this.volume  //le volume est configuré de 0.1 à 1
      });
    }

  }
  volup(){
    this.volume = this.volume + 0.1;
    if(this.volume <= 1){
      NativeAudio.setVolume({
        assetId: "assetId",
        volume: this.volume  //le volume est configuré de 0.1 à 1
      });
    }

  }

  next(){
    this.stop();
    this.route.navigate(['/lecture', parseInt(this.idSong)+1]);
    const play = document.getElementById("play");
    // @ts-ignore
    play.style.display = "inherit";
    const pause = document.getElementById("pause");
    // @ts-ignore
    pause.style.display = "none";

  }
  prev(){
    this.stop();
    this.route.navigate(['/lecture', parseInt(this.idSong)-1]);
    const play = document.getElementById("play");
    // @ts-ignore
    play.style.display = "inherit";
    const pause = document.getElementById("pause");
    // @ts-ignore
    pause.style.display = "none";

  }

  loop(){
    NativeAudio.loop({
      assetId: 'assetId',
    });

  }

  protected readonly parseInt = parseInt;
}