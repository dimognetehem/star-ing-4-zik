import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.dimognemusicapp.ing4isi',
  appName: 'Star ING4 Zik',
  webDir: 'www',
  server: {
    androidScheme: 'https'
  }
};

export default config;
